// respond to button click
console.log("Page load happened!")

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from title, author and story
    var planned_text = document.getElementById('plan-text').value;
    var location_text = document.getElementById('location-text').value;
    var plan_text = document.getElementById('text-plan').value;
    var comp_string = "Y"
    if (plan_text === comp_string) {
        plan_text = "They do have a full plan."
    }
    else {
        plan_text = "They're still figuring ot their plan but they have an idea."
    }
    console.log('plan:' + planned_text + ' location: ' + location_text + ' plan talk: ' + plan_text);

    // get checkbox state
    var plan_string = "";
    if (document.getElementById('checkbox-travel-value').checked){
        console.log('Traveling over break!');
        plan_string += "Traveling over break, ";
    }

    if (document.getElementById('checkbox-family-value').checked) {
        console.log('String with family over break!');
        plan_string += "Staying with Family, ";
    }

    if (document.getElementById('checkbox-winter-class-value').checked) {
        console.log('Taking a class over winter break!');
        plan_string += "Winter Class";
    }
    // make genre combined string
    console.log('Plans: ' + plan_string);

    // make dictionary
    plan_details = {};
    plan_details['plans?'] = planned_text;
    plan_details['location'] = location_text;
    plan_details['plans'] = plan_text;
    plan_details['travel'] = plan_string;
    console.log(plan_details);

    displayPlan(plan_details);

}

function displayPlan(plan_details){
    console.log('entered displayPlan!');
    console.log(plan_details);
    // get fields from plan and display in label.
    var plan_top = document.getElementById('plan-top-line');
    plan_top.innerHTML = "Your plans have been submitted";

}
